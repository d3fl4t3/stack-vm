from opcodes import OpCodes, DATA_LENGTH


class Interpreter:
    def __init__(self, code):
        self.code = code
        self.stack = []
        self.pos = 0

    def read_op(self):
        op = OpCodes(self.code[self.pos])
        self.pos += 1
        data = 0
        if op in DATA_LENGTH:
            for i in range(DATA_LENGTH[op]):
                data += 256 ** i * self.code[self.pos]
                self.pos += 1
        return op, data

    def push_stack(self, data):
        self.stack.append(data)

    def pop_stack(self):
        return self.stack.pop()

    def exec_op(self, op, data):
        print(op, data)
        if op == OpCodes.NOP:
            return
        elif op in [OpCodes.PUSH_BYTE, OpCodes.PUSH_WORD, OpCodes.PUSH_DWORD, OpCodes.PUSH_QWORD]:
            self.push_stack(data)
        elif op in [OpCodes.BINARY_ADD, OpCodes.BINARY_DIV, OpCodes.BINARY_MOD, OpCodes.BINARY_MUL, OpCodes.BINARY_SUB]:
            operand_r = self.pop_stack()
            operand_l = self.pop_stack()
            if op == OpCodes.BINARY_ADD:
                self.push_stack(operand_l + operand_r)
            elif op == OpCodes.BINARY_SUB:
                self.push_stack(operand_l - operand_r)
            elif op == OpCodes.BINARY_MUL:
                self.push_stack(operand_l * operand_r)
            elif op == OpCodes.BINARY_DIV:
                self.push_stack(operand_l // operand_r)
            elif op == OpCodes.BINARY_MOD:
                self.push_stack(operand_l % operand_r)

    def run(self):
        while self.pos != len(self.code):
            op, data = self.read_op()
            self.exec_op(op, data)
        print(self.stack)
