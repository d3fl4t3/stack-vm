#!/usr/bin/env python3
from sys import argv, exit
import os

from interpreter import Interpreter
from translator import Translator


def run_file(file):
    with open(file, 'rb') as f:
        code = f.read()
    interp = Interpreter(code)
    interp.run()

def compile_file(file):
    with open(file, 'r') as f:
        t = Translator(src=f.read())
    code = t.assemble()
    with open(os.path.splitext(file)[0], 'wb') as f:
        f.write(code)
    print("OK")

def decompile_file(file):
    with open(file, 'rb') as f:
        t = Translator(code=f.read())
    print(t.disassemble(), end='')

if __name__ == "__main__":
    if len(argv) == 3:
        if argv[1] == 'run':
            run_file(argv[2])
        elif argv[1] == 'compile':
            compile_file(argv[2])
        elif argv[1] == 'decompile':
            decompile_file(argv[2])
        else:
            print("Unknown command: {}".format(argv[1]))
            exit(1)
    else:
        print("Usage: {} <command> <file>".format(argv[0]))
        exit(1)
