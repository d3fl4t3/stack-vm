from enum import Enum


class OpCodes(Enum):
    NOP = 0x00
    BINARY_ADD = 0x01
    BINARY_SUB = 0x02
    BINARY_MUL = 0x03
    BINARY_DIV = 0x04
    BINARY_MOD = 0x05
    PUSH_BYTE = 0x06
    PUSH_WORD = 0x07
    PUSH_DWORD = 0x08
    PUSH_QWORD = 0x09


DATA_LENGTH = {
    OpCodes.PUSH_BYTE: 1,
    OpCodes.PUSH_WORD: 2,
    OpCodes.PUSH_DWORD: 4,
    OpCodes.PUSH_QWORD: 8,
}
