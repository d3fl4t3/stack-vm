from opcodes import OpCodes, DATA_LENGTH
import re

SRC_REGEX = '(?im)^\s*(push|add|sub|mul|div|mod|nop)\s*(\d+)?\s*$'


class Translator:
    def __init__(self, src='', code=b''):
        self.src = src
        self.code = code

    def assemble(self):
        assert re.match(SRC_REGEX, self.src), 'Parse error'
        commands = re.findall(SRC_REGEX, self.src)
        code = bytearray()
        for command in commands:
            op = command[0].upper()
            if op == "PUSH":
                assert len(command) == 2, 'Data must be passed as PUSH argument'
                data = int(command[1])
                assert data < 256 ** 8, 'Data must fit in QWORD'
                if data >= 256 ** 4:
                    op = "PUSH_QWORD"
                    data_size = 8
                elif data >= 256 ** 2:
                    op = "PUSH_DWORD"
                    data_size = 4
                elif data >= 256:
                    op = "PUSH_WORD"
                    data_size = 2
                else:
                    op = "PUSH_BYTE"
                    data_size = 1
                data = data.to_bytes(data_size, 'little')
                code += bytearray([OpCodes[op].value])
                code += data
            elif op == "NOP":
                code += bytearray([OpCodes[op].value])
            else:
                code += bytearray([OpCodes["BINARY_" + op].value])
        return code

    def disassemble(self, pretty=True):
        pos = 0
        src = ''
        while pos != len(self.code):
            op = OpCodes(self.code[pos])
            if pretty and op.name.startswith('PUSH'):
                src += 'PUSH'
            elif pretty and op.name.startswith('BINARY_'):
                src += op.name[7:]
            else:
                src += op.name
            pos += 1
            if op in DATA_LENGTH:
                data = int.from_bytes(self.code[pos:pos + DATA_LENGTH[op]], 'little')
                src += " {}".format(data)
                pos += DATA_LENGTH[op]
            src += '\n'
        return src
